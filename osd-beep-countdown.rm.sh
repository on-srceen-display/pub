#!/bin/bash
[ -z $1 ] &&printf '\n\n$0 $loops $loop_length $x $y $tag\n\n'
[ -z $1 ] &&x=10
[ -z $2 ] &&x=60
[ -z $3 ] &&x=700
[ -z $4 ] &&x=500
i=$1
while [ "$i" -ne "0" ] ; do 
	( /mnt/c/Program\ Files/JPSoft/TCC27/tcc.exe osd /pos=$3,$4 /time=$2 ${i}$5 >n ) &
	sleep $2
i=$[$i-1]
done
#printf '\n\nTimes up...\n\n'
powershell.exe '[console]::beep(161.6,1100)'
powershell.exe '[console]::beep(261.6,700)'
powershell.exe '[console]::beep(361.6,500)'
powershell.exe '[console]::beep(461.6,140)'
powershell.exe '[console]::beep(561.6,380)'
powershell.exe '[console]::beep(161.6,1100)'
powershell.exe '[console]::beep(661.6,240)'
powershell.exe '[console]::beep(161.6,1100)'
powershell.exe '[console]::beep(761.6,180)'
powershell.exe '[console]::beep(161.6,1100)'
powershell.exe '[console]::beep(861.6,120)'
powershell.exe '[console]::beep(161.6,1100)'
powershell.exe '[console]::beep(961.6,080)'
powershell.exe '[console]::beep(161.6,1100)'
powershell.exe '[console]::beep(1061.6,1100)'
_beep () {
	  powershell.exe "[console]::beep($1,$2)"
  }

alias bleep="_beep 1000 800"  # A strong bleep (for profanity)
alias  beep="_beep 2000 300"  # Quick yet noticeable beep
alias  blip="_beep 4000  80"  # A less distracting blip
